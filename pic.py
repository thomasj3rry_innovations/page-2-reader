import cv2
import tkinter as tk
from tkinter import ttk

class PictureTaker:
    def __init__(self):
        self.window = tk.Tk()
        self.window.title("Picture Taker")
        
        self.camera = cv2.VideoCapture(0)
        if not self.camera.isOpened():
            print("Error: Could not open camera.")
            return
        
        self.current_frame = None
        
        self.canvas = tk.Canvas(self.window, width=640, height=480)
        self.canvas.grid(row=0, column=0, columnspan=1)
        
        self.save_button = ttk.Button(self.window, text="Save", command=self.save_image)
        self.save_button.grid(row=1, column=0, sticky="s")
        
        self.update()
        
        self.window.mainloop()
    
    def update(self):
        ret, frame = self.camera.read()
        if ret:
            self.current_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            self.photo = self.convert_frame_to_photo(self.current_frame)
            self.canvas.create_image(0, 0, image=self.photo, anchor=tk.NW)
        self.window.after(10, self.update)
    
    def convert_frame_to_photo(self, frame):
        height, width, channels = frame.shape
        image = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        return tk.PhotoImage(data=cv2.imencode('.png', image)[1].tobytes())
    
    def save_image(self):
        if self.current_frame is not None:
            filename = "captured_image.jpg"
            cv2.imwrite(filename, cv2.cvtColor(self.current_frame, cv2.COLOR_RGB2BGR))
            print("Image saved as", filename)


if __name__ == "__main__":
    # root = tk.Tk()
    # app = PictureTaker(root, "Picture Taker App")
    PictureTaker()
