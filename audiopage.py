import re
from pdf2image import convert_from_path
import cv2
import pytesseract
import os
from gtts import gTTS
from datetime import datetime
import pygame
import tkinter as tk
from tkinter import filedialog, messagebox
from tkinter import ttk

# Global variables
image_file_path = ''
converted_text = ''
audio_file_path = ''
saved_file = ''
lang_var = ''
u_lang = ''
selected_lang = ''
extracted_text = ''
is_paused = False
audio_path_var = ""

# Function to handle picture taking with a custom class
class PictureTaker:
    def __init__(self):
        self.window = tk.Toplevel()  # Use Toplevel to create a new window
        self.window.title("Picture Taker")
        
        self.camera = cv2.VideoCapture(0)
        if not self.camera.isOpened():
            print("Error: Could not open camera.")
            return
        
        self.current_frame = None
        
        self.canvas = tk.Canvas(self.window, width=640, height=480)
        self.canvas.grid(row=0, column=0, columnspan=1)
        
        self.save_button = ttk.Button(self.window, text="Save", command=self.save_image)
        self.save_button.grid(row=1, column=0, sticky="s")
        
        self.update()
        
        self.window.mainloop()
    
    def update(self):
        ret, frame = self.camera.read()
        if ret:
            self.current_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            self.photo = self.convert_frame_to_photo(self.current_frame)
            self.canvas.create_image(0, 0, image=self.photo, anchor=tk.NW)
        self.window.after(10, self.update)
    
    def convert_frame_to_photo(self, frame):
        height, width, channels = frame.shape
        image = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        return tk.PhotoImage(data=cv2.imencode('.png', image)[1].tobytes())
    
    def save_image(self):
        global image_file_path
        if self.current_frame is not None:
            image_file_path = "captured_image.jpg"
            cv2.imwrite(image_file_path, cv2.cvtColor(self.current_frame, cv2.COLOR_RGB2BGR))
            print("Image saved as", image_file_path)
            self.window.destroy()  # Close the PictureTaker window after saving
            image_path_var.set(image_file_path)

def extract_image_to_text(image_file_path, lang='eng'):
    image = cv2.imread(image_file_path)
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    extracted_text = pytesseract.image_to_string(gray_image, lang=lang)

    # Using re.sub to replace multiple patterns
    replacements = {
        r'\|': 'i',
        r'—': ' ',
        r'\n': ' ',
        r' {2,}': ' ',  # Replace multiple spaces with a single space
        r'_': ' '
    }

    for pattern, replacement in replacements.items():
        extracted_text = re.sub(pattern, replacement, extracted_text)

    return extracted_text

def text_to_speech(extracted_text, selected_lang, image_path_var):
    tts = gTTS(text=extracted_text, lang=selected_lang)
    """ #if user want to save with today's date and time 
    now = datetime.now()
    formatted_date = now.strftime("%Y_%m_%d")
    os.makedirs(formatted_date, exist_ok=True)
    formatted_datetime = now.strftime("%Y_%m_%d_%H_%M_%S")
    saved_file = os.path.join(formatted_date, f'{formatted_datetime}.mp3')
    tts.save(saved_file)
    return saved_file """
    audio_path_var = re.sub('.jpg', '.mp3', image_path_var)
    tts.save(audio_path_var)
    return audio_path_var

def play_sound(file_path):
    global is_paused
    pygame.mixer.init()
    pygame.mixer.music.load(file_path)
    pygame.mixer.music.play()
    is_paused = False
    update_pause_button()

def toggle_pause_unpause():
    global is_paused
    if is_paused:
        pygame.mixer.music.unpause()
        is_paused = False
    else:
        pygame.mixer.music.pause()
        is_paused = True
    update_pause_button()

def update_pause_button():
    if is_paused:
        pause_button.config(text="▶ Unpause Audio")
    else:
        pause_button.config(text="⏸ Pause Audio")

# Initialize Tkinter
root = tk.Tk()
root.title("Multi-Step Application")
root.geometry("500x400")

# Center the window
window_width = 800
window_height = 450
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
position_top = int(screen_height / 2 - window_height / 2)
position_right = int(screen_width / 2 - window_width / 2)
root.geometry(f"{window_width}x{window_height}+{position_right}+{position_top}")

# Function to switch frames
def show_frame(frame):
    frame.tkraise()

# Initialize the StringVar with a default value
var = tk.StringVar(value="none")
image_path_var = tk.StringVar(value="")

# Function to handle language selection
def print_selection(*args):
    global lang_var, u_lang
    lang_var = var.get()
    u_lang = {'en': 'eng', 'fr': 'fra', 'te': 'tel', 'es': 'spa', 'hi': 'hin', 'ru': 'rus'}[lang_var]

var.trace_add("write", print_selection)

# Create frames for different pages
frame1, frame2, frame3 = tk.Frame(root), tk.Frame(root), tk.Frame(root)

for frame in (frame1, frame2, frame3):
    frame.grid(row=0, column=0, sticky='nsew')

# Page 1: Language Selection
def setup_page1():
    tk.Label(frame1, text="Select a Language").pack(pady=10, padx=320)
    languages = [("English", "en"), ("French", "fr"), ("Telugu", "te"), ("Spanish", "es"), ("Hindi", "hi"), ("Russian", "ru")]
    for (text, value) in languages:
        tk.Radiobutton(frame1, text=text, padx=20, variable=var, value=value).pack(pady=5)
    tk.Button(frame1, text="Next", command=lambda: show_frame(frame2)).pack(pady=20)

############## this code is for next update ################

############## this code is for next update ################

# Function to select an image file
def select_file():
    global image_file_path
    image_file_path = filedialog.askopenfilename(filetypes=[("Image files", "*.jpg *.jpeg *.png"), ("All files", "*.*")])
    if image_file_path:
        print("Selected file:", image_file_path)
        image_path_var.set(image_file_path)
    else:
        print("No file selected.")

def display_audio_file():
    global audio_file_path
    if audio_file_path:
        print("Audio file:", audio_file_path)
        return audio_file_path
    else:
        print("No audio file selected.")
        return None

# Page 2: Select Image or PDF File
def setup_page2():
    tk.Label(frame2, text="You have selected: ").pack(pady=1)
    tk.Label(frame2, textvariable=var).pack(pady=0)
    
    tk.Label(frame2, text="If you have an image file").pack(pady=10, padx=170)
    tk.Button(frame2, text="Select Image or Pdf File", command=select_file).pack(pady=5)
    
    tk.Label(frame2, text="Don't a softcopy? Don't worry").pack(pady=10, padx=170)
    tk.Button(frame2, text="Take a picture", command=PictureTaker).pack(pady=5)
    
    tk.Button(frame2, text="Next", command=lambda: show_frame(frame3)).pack(pady=20)
    tk.Button(frame2, text="Back", command=lambda: show_frame(frame1)).pack(pady=5)

# Page 3: Display and Convert
def setup_page3():
    global image_file_path, extracted_text, saved_file

    def convert_and_display():
        global extracted_text, saved_file, audio_path_var
        extracted_text = extract_image_to_text(image_file_path, lang=u_lang)

        audio_path_var = image_path_var.get().replace('.jpg', '.mp3')
  
        print(audio_path_var)

        selected_lang = var.get()
        saved_file = text_to_speech(extracted_text, selected_lang, audio_path_var)
        audio_label.config(text=f"{saved_file}")

    tk.Label(frame3, text="Selected File:").pack(pady=5)   
    tk.Label(frame3, textvariable=image_path_var).pack(pady=5)

    tk.Button(frame3, text="Convert to Audio", command=convert_and_display).pack(pady=5)
    
    global audio_label
    tk.Label(frame3, text="Audio file saved at: ").pack(pady=5)
    audio_label = tk.Label(frame3, text="")
    audio_label.pack(pady=10)

    tk.Button(frame3, text="Preview Audio", command=lambda: play_sound(saved_file)).pack(pady=5)
    
    global pause_button
    pause_button = tk.Button(frame3, text="⏸ Pause Audio", command=toggle_pause_unpause)
    pause_button.pack(pady=5)

    tk.Button(frame3, text="Back", command=lambda: show_frame(frame2)).pack(pady=20)

    tk.Label(frame3, text="wanna try another?").pack(pady=5)
    tk.Button(frame3, text="Next", command=lambda: show_frame(frame1)).pack(pady=20)

# Setup pages
setup_page1()
setup_page2()
setup_page3()

# Show the first frame initially
show_frame(frame1)

# Run the application
root.mainloop()
