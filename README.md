# Page 2 Reader

## why to use this?
### you don't need pay for the audio books anymore
### coz there's no need for audio books
### if you use this book you can to create your own audio books

## how to use
 run the program 

 in the first page select which language you want the program to read

 click next

 in the next page. check if your choice of language is correct or not

 click on the 

 ### "Select image file" button 

 and after selection click the next button

 there in the next page ensure the image file is correct and click on

 ### 'Convert to audio' button

 it will show the generated audio file path, you can check that file path

 you can preview the audio using the buttons placed there


 ## pictures with image are supported  but not advisable because it will skip some lines
## hand written texts are not supported, our system can't detect the written words

## installing 

v = 1/2/3

### python[v] -m pip install requirements.txt # if you use vscode with jupyter extension you can also install the jupy_vscode_requirements.txt 

or just follow the below installation

pip install opencv-python pytesseract pdf2image gtts pygame


<!-- if want both the image and camera #opencv-python -->
<!-- if only want image #pillow -->


### if user is linux 
sudo apt-get install -y espeak-ng ffmpeg alsa-utils

<!-- pip install larynx -->

<!-- pip install torch transformers  -->


<!-- pip install pyqt5 -->

 <!-- pip install langdetect -->





